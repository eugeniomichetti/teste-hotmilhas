FROM romeoz/docker-nginx-php:7.1

RUN apt-get update \
    && apt-get install -y iproute2 \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /var/www/app

COPY ./ ./

COPY www.conf /etc/php/7.1/fpm/pool.d/www.conf
