<?php

namespace App;


class Route
{
    private $routes;

    public function __construct()
    {
        $this->initRoutes();
        $this->run($this->getUrl());
    }

    public function initRoutes()
    {
        $routes['index'] = array('route' => '/', 'controller' => 'IndexController', 'action' => 'index');

        $routes['account'] = array('route' => '/account', 'controller' => 'AccountController', 'action' => 'index');
        $routes['account.add'] = array('route' => '/account/new', 'controller' => 'AccountController', 'action' => 'create');
        $routes['account.delete'] = array('route' => '/account/delete', 'controller' => 'AccountController', 'action' => 'delete');

        $routes['client'] = array('route' => '/client', 'controller' => 'ClientController', 'action' => 'index');
        $routes['client.add'] = array('route' => '/client/new', 'controller' => 'ClientController', 'action' => 'create');
        $routes['client.delete'] = array('route' => '/client/delete', 'controller' => 'ClientController', 'action' => 'delete');
        $routes['client.update'] = array('route' => '/client/update', 'controller' => 'ClientController', 'action' => 'update');

        $routes['transaction'] = array('route' => '/transaction', 'controller' => 'TransactionController', 'action' => 'index');
        $routes['transaction.add'] = array('route' => '/transaction/new', 'controller' => 'TransactionController', 'action' => 'create');

        $routes['extract'] = array('route' => '/report', 'controller' => 'ReportController', 'action' => 'index');
        $routes['extract.report'] = array('route' => '/report/get', 'controller' => 'ReportController', 'action' => 'getReport');

        $this->setRoutes($routes);
    }

    public function run($url)
    {
        array_walk($this->routes, function ($route) use ($url) {
            if ($url == $route['route']) {
                $class = "App\\Controllers\\" . $route['controller'];
                $controller = new $class;
                $action = $route['action'];
                $controller->$action();
            }
        });
    }

    public function setRoutes(array $routes)
    {
        $this->routes = $routes;
    }

    public function getUrl()
    {
        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }
}