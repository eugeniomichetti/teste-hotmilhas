<h2>Relatório</h2>
<form action="/report/get" method="post">
    <div class="form-group">
        <label>Data inicial</label>
        <input name="dateFrom" class="form-control" type="date" placeholder="Selecione a data">
    </div>
    <div class="form-group">
        <label for="">Data final</label>
        <input name="dateTo" class="form-control" type="date" placeholder="Selecione a data">
    </div>

    <button type="submit" class="btn btn-primary">Buscar</button>
</form>

<?php if (isset($this->view->reports) && count($this->view->reports) > 0): ?>
    <div class="table-responsive">
        <br><BR>
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Conta</th>
                <th>Tipo</th>
                <th>Valor</th>
                <th>Status</th>
                <th>Data prevista</th>
                <th>Data realizada</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($this->view->reports as $report): ?>
                <tr>
                    <td> <?= $report['id'] ?> </td>
                    <td> <?= $report['account_id'] ?> </td>
                    <td> <?= $report['type'] ?> </td>
                    <td> <?= $report['price'] ?> </td>
                    <td> <?= $report['status'] ?> </td>
                    <td> <?= $report['expected_date'] ?> </td>
                    <td> <?= $report['release_date'] ?> </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<? elseif (count($this->view->reports) <= 0): ?>
<br><BR>
    <p> Não foram encontrados registros para o intervalo selecionado.</p>
<?php endif; ?>