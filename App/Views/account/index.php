<h2>Contas</h2>
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#newAccount">
    Adicionar nova conta
</button>

<a href="/transaction" class="btn btn-primary"> Realizar transação</a>

<div class="table-responsive">
    <br><BR>
    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>Usuário</th>
            <th>Saldo</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($this->view->accounts as $account): ?>
            <tr>
                <td> <?= $account['account_id'] ?> </td>
                <td> <?= $account['name'] ?> </td>
                <td> <?= $account['balance'] ?> </td>
                <td>
                    <button type="button" class="deleteAccount btn btn-danger btn-sm" data-toggle="modal"
                            data-target="#deleteAccount" data-id="<?= $account['account_id'] ?>">Excluir
                    </button>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="modal fade" id="newAccount" tabindex="-1" role="dialog" aria-labelledby="newAccountLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newAccountLabel">Novo usuário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" action="/account/new" method="post" novalidate>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Usuário</label>
                        <select class="form-control" name="client_id">
                            <?php foreach ($this->view->clients as $client): ?>
                                <option value="<?= $client['id'] ?>"><?= $client['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteAccount" tabindex="-1" role="dialog" aria-labelledby="deleteAccountLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteAccountLabel">Deletar usuário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" action="/account/delete" method="post" novalidate>
                <div class="modal-body">
                    <input type="hidden" id="account_id" name="account_id" value="">
                    <p>Tem certeza que deseja deletar esta conta? </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger">Deletar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).on("click", ".deleteAccount", function () {
        var account_id = $(this).data('id');
        $('#account_id').val(account_id);
    });
</script>