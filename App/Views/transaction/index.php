<h2>Transação</h2>
<form action="/transaction/new" method="post">
    <div class="form-group">
        <label>Conta</label>
        <select class="form-control" name="account_id">
            <?php foreach ($this->view->accounts as $account): ?>
                <option value="<?= $account['account_id'] ?>"><?= $account['account_id'] . " - " . $account['name'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="">Tipo transação</label>
        <select class="form-control" name="type">
            <option value="deposito"> Depósito</option>
            <option value="transferencia"> Transferência</option>
        </select>
    </div>
    <div class="form-group">
        <label for="">Data esperada</label>
        <input name="expected_date" class="form-control" type="date" placeholder="Selecione a data">
    </div>
    <div class="form-group">
        <label for="">Valor da transação</label>
        <input name="price" class="form-control" type="number">
    </div>

    <button type="submit" class="btn btn-primary">Confirmar</button>
</form>