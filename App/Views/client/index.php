<h2>Clientes</h2>
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#newClient">
    Adicionar novo cliente
</button>
<div class="table-responsive">
    <br><BR>
    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($this->view->clients as $client): ?>
            <tr>
                <td> <?= $client['id'] ?> </td>
                <td> <?= $client['name'] ?> </td>
                <td> <?= $client['email'] ?> </td>
                <td>
                    <button type="button" class="updateClient btn btn-primary btn-sm" data-toggle="modal"
                            data-target="#updateClient" data-id="<?= $client['id'] ?>"
                            data-name="<?= $client['name'] ?>" data-email="<?= $client['email'] ?>">Editar
                    </button>

                    <button type="button" class="deleteUser btn btn-danger btn-sm" data-toggle="modal"
                            data-target="#deleteClient" data-id="<?= $client['id'] ?>">Excluir
                    </button>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="modal fade" id="newClient" tabindex="-1" role="dialog" aria-labelledby="newClientLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newClientLabel">Novo usuário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" action="/client/new" method="post" novalidate>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Nome</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Digite o nome"
                               value="" required>
                        <div class="invalid-feedback">
                            Preencha o campo nome.
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Digite o e-mail"
                               value=""
                               required>
                        <div class="invalid-feedback">
                            Preencha o campo e-mail.
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="updateClient" tabindex="-1" role="dialog" aria-labelledby="updateClientLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateClientLabel">Editar usuário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" action="/client/update" method="post" novalidate>
                <div class="modal-body">
                    <input type="hidden" id="updateId" name="id" value="">
                    <div class="form-group">
                        <label for="name">Nome</label>
                        <input type="text" class="form-control" id="updateName" name="name" placeholder="Digite o nome"
                               value="" required>
                        <div class="invalid-feedback">
                            Preencha o campo nome.
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" id="updateEmail" name="email"
                               placeholder="Digite o e-mail"
                               value=""
                               required>
                        <div class="invalid-feedback">
                            Preencha o campo e-mail.
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteClient" tabindex="-1" role="dialog" aria-labelledby="deleteClientLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteClientLabel">Deletar usuário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" action="/client/delete" method="post" novalidate>
                <div class="modal-body">
                    <input type="hidden" id="user_id" name="user_id" value="">
                    <p>Tem certeza que deseja deletar este usuário? </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger">Deletar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).on("click", ".deleteUser", function () {
        var user_id = $(this).data('id');
        $('#user_id').val(user_id);
    });

    $(document).on("click", ".updateClient", function () {
        var user_id = $(this).data('id'),
            user_name = $(this).data('name'),
            user_email = $(this).data('email');

        $('#updateId').val(user_id);
        $('#updateName').val(user_name);
        $('#updateEmail').val(user_email);
    });

</script>