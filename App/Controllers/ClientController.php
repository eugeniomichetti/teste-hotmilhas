<?php
namespace App\Controllers;

use App\Conn;
use App\Models\Client;

class ClientController
{
    protected $view;

    public function __construct()
    {
        $this->view = new \stdClass;
    }

    public function index()
    {
        $clients = new Client(Conn::getDB());
        $this->view->clients = $clients->all();

        $this->render();
    }

    public function create()
    {
        $client = new Client(Conn::getDB());
        $client->setName(isset($_POST['name']) ? $_POST['name'] : NULL);
        $client->setEmail(isset($_POST['email']) ? $_POST['email'] : NULL);

        $client->create($client);

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function delete()
    {
        $client = new Client(Conn::getDB());
        if (isset($_POST['user_id'])) {
            $client->delete($_POST['user_id']);
        }

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function update()
    {
        $client = new Client(Conn::getDB());
        if (count($client->find($_POST['id'])) > 0) {
            $client->setId(isset($_POST['id']) ? $_POST['id'] : NULL);
            $client->setName(isset($_POST['name']) ? $_POST['name'] : NULL);
            $client->setEmail(isset($_POST['email']) ? $_POST['email'] : NULL);

            $client->update($client);
        }

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function render()
    {
        include_once "../App/Views/template/layout.php";
    }

    public function content()
    {
        include_once "../App/Views/client/index.php";
    }
}