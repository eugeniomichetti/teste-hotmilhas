<?php

namespace App\Controllers;

use App\Conn;
use App\Models\Report;

class ReportController
{
    protected $view;

    public function __construct()
    {
        $this->view = new \stdClass;
    }

    public function index()
    {
        $this->render();
    }

    public function getReport()
    {
        $reports = new Report(Conn::getDB());
        $this->view->reports = $reports->reports($_POST['dateFrom'], $_POST['dateTo']);
        $this->render();
    }

    public function render()
    {
        include_once "../App/Views/template/layout.php";
    }

    public function content()
    {
        include_once "../App/Views/report/index.php";
    }
}