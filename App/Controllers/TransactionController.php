<?php

namespace App\Controllers;


use App\Models\Account;
use App\Conn;
use App\Models\Transaction;

class TransactionController
{
    protected $view;

    public function __construct()
    {
        $this->view = new \stdClass;
    }

    public function index()
    {
        $accounts = new Account(Conn::getDB());
        $this->view->accounts = $accounts->all();

        $this->render();
    }

    public function create()
    {
        $transaction = new Transaction(Conn::getDB());
        $transaction->setAccountId(isset($_POST['account_id']) ? $_POST['account_id'] : NULL);
        $transaction->setExpectedDate(isset($_POST['expected_date']) ? $_POST['expected_date'] : NULL);
        $transaction->setPrice(isset($_POST['price']) ? $_POST['price'] : NULL);
        $transaction->setReleaseDate(date("Y-m-d"));
        $transaction->setType(isset($_POST['type']) ? $_POST['type'] : NULL);
        $transaction->setStatus("aprovado");

        $transaction->create($transaction);

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function render()
    {
        include_once "../App/Views/template/layout.php";
    }

    public function content()
    {
        include_once "../App/Views/transaction/index.php";
    }
}