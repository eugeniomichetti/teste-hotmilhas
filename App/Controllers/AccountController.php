<?php

namespace App\Controllers;


use App\Conn;
use App\Models\Account;
use App\Models\Client;

class AccountController
{
    protected $view;

    public function __construct()
    {
        $this->view = new \stdClass;
    }

    public function index()
    {
        $accounts = new Account(Conn::getDB());
        $clients = new Client(Conn::getDB());

        $this->view->accounts = $accounts->all();
        $this->view->clients = $clients->all();

        $this->render();
    }

    public function create()
    {
        $account = new Account(Conn::getDB());
        $account->setClientId(isset($_POST['client_id']) ? $_POST['client_id'] : NULL);
        $account->setBalance(isset($_POST['balance']) ? $_POST['balance'] : 0.0);

        $account->create($account);

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function delete()
    {

        $account = new Account(Conn::getDB());
        if (isset($_POST['account_id'])) {
            $account->delete($_POST['account_id']);
        }

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function update()
    {
        $account = new Account(Conn::getDB());
        if (count($account->find($_POST['id'])) > 0) {
            $account->setId(isset($_POST['id']) ? $_POST['id'] : NULL);
            $account->setName(isset($_POST['name']) ? $_POST['name'] : NULL);
            $account->setEmail(isset($_POST['email']) ? $_POST['email'] : NULL);

            $account->update($account);
        }

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function render()
    {
        include_once "../App/Views/template/layout.php";
    }

    public function content()
    {
        include_once "../App/Views/account/index.php";
    }
}