<?php

namespace App\Models;


class Client
{
    protected $db;
    private $id;
    private $name;
    private $email;


    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function create(Client $client)
    {
        $query = "insert into users (name, email) value (?, ?)";
        $stmt = $this->db->prepare($query);

        $stmt->execute(array($client->getName(), $client->getEmail()));
    }

    public function all()
    {
        $query = "select * from users";
        return $this->db->query($query);
    }

    public function find($id)
    {
        $query = "select * from users where id=:id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":id", $id);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function delete($id)
    {
        $query = "delete from users where id=:id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":id", $id);

        $stmt->execute();
    }

    public function update(Client $client)
    {
        $query = "UPDATE users SET `name` = :client_name, email= :client_email WHERE id = :client_id";

        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":client_id", $client->getId());
        $stmt->bindParam(":client_name", $client->getName());
        $stmt->bindParam(":client_email", $client->getEmail());

        $stmt->execute();
    }
}