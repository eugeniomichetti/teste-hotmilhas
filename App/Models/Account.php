<?php

namespace App\Models;


class Account
{
    protected $db;

    protected $client_id;
    protected $balance;

    public function getClientId()
    {
        return $this->client_id;
    }

    public function setClientId($client_id)
    {
        $this->client_id = $client_id;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function create(Account $account)
    {
        $query = "insert into accounts (client_id, balance) value (?, ?)";
        $stmt = $this->db->prepare($query);

        $stmt->execute(array($account->getClientId(), $account->getBalance()));
    }

    public function all()
    {
        $query = "select accounts.id as account_id, accounts.balance as balance, users.name as name from accounts INNER JOIN users ON accounts.client_id = users.id";
        return $this->db->query($query);
    }

    public function delete($id)
    {
        $query = "delete from accounts where id=:id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":id", $id);
        $stmt->execute();;
    }

}