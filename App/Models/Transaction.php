<?php

namespace App\Models;


class Transaction
{
    protected $db;
    private $type;
    private $account_id;
    private $price;
    private $status;
    private $expected_date;
    private $release_date;

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getAccountId()
    {
        return $this->account_id;
    }

    public function setAccountId($account_id)
    {
        $this->account_id = $account_id;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getExpectedDate()
    {
        return $this->expected_date;
    }

    public function setExpectedDate($expected_date)
    {
        $this->expected_date = $expected_date;
    }

    public function getReleaseDate()
    {
        return $this->release_date;
    }

    public function setReleaseDate($release_date)
    {
        $this->release_date = $release_date;
    }

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function create(Transaction $transaction)
    {
//        var_dump($transaction);
        $query = "insert into transactions (type, account_id, price, status, expected_date, release_date) value (?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);

        $stmt->execute(array($transaction->getType(), $transaction->getAccountId(), $transaction->getPrice(), $transaction->getStatus(), $transaction->getExpectedDate(), $transaction->getReleaseDate()));
    }

    public function all()
    {
        $query = "select * from transaction";
        return $this->db->query($query);
    }
}