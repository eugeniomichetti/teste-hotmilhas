<?php

namespace App\Models;


class Report
{
    protected $db;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function reports($dateFrom, $dateTo)
    {
        $query = "SELECT * FROM transactions WHERE release_date BETWEEN :dateFrom AND :dateTo";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":dateFrom", $dateFrom);
        $stmt->bindParam(":dateTo", $dateTo);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function all()
    {
        $query = "select * from users";
        return $this->db->query($query);
    }
}