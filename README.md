#Teste Hotmilhas
Foi desnvolvido uma aplicação utilizando PHP 7 com a estrutura MVC. O sistema possui um CRUD de contas, usuários e possui uma view de realização de transações e uma view para visualização de relatórios.

#Como utilizar
Para a utilização deste sistema é necessário um servidor PHP instalado com uma versão acima da 5.6. 
Realize a instalção do Composer (https://getcomposer.org/) caso não possua. 

Após a instalção dos programas acima siga os passos abaixo:

**1-** Importe o arquivo .sql que se encontra na pasta "teste-hotmilhas";

**2-** Rode o comando composer install

**3-** Inicie o servidor do PHP com o comando "php -S localhost:8000"

**4-** Abra o navegador para visualizar o sistema.

